# SQLCipher Qt Example
This is a very simple (and useless) Qt application that implements the
[SQLCipher](https://github.com/sqlcipher/sqlcipher) encrypted database library.

It makes use of the [sjemens/qsqlcipher-qt5](https://github.com/sjemens/qsqlcipher-qt5)
plugin to introduce the *SQLCipher* database driver to the Qt Framework.

## Preparations
In order to use this plugin you first need to install it to your system.
Just follow [the developer's instructions](https://github.com/sjemens/qsqlcipher-qt5#build-instructions-without-the-tests)
to do so.
Note that *SQLCipher* will automatically be installed alongside with this plugin
so you don't need to install it separately.

## Build
Simple and straightforward:
```bash
git clone https://codeberg.org/Pik-9/SQLCipherExample.git
cd SQLCipherExample
mkdir build
cd build
qmake ..
make
```

## Caveats
This compiled binary will only run on systems that have the *sjemens/qsqlcipher-qt5*
plugin installed. Statically compiling a Qt5 application for Linux is possible, but
cumbersome since you need to manually compile the whole Qt Framework.

## Cross Compiling with mxe.cc
Though this should be possible, this has not been explored, yet.
