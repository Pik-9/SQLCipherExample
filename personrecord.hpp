/*************************************************************************************
 * Copyright (c) 2023 Daniel Steinhauer                                              *
 *                                                                                   *
 * Permission is hereby granted, free of charge, to any person obtaining a copy      *
 * of this software and associated documentation files (the "Software"), to deal     *
 * in the Software without restriction, including without limitation the rights      *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         *
 * copies of the Software, and to permit persons to whom the Software is             *
 * furnished to do so, subject to the following conditions:                          *
 *                                                                                   *
 * The above copyright notice and this permission notice shall be included in all    *
 * copies or substantial portions of the Software.                                   *
 *                                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     *
 * SOFTWARE.                                                                         *
 *************************************************************************************/

#ifndef PERSONRECORD_HPP
#define PERSONRECORD_HPP

#include <string>
#include <vector>

#include <QString>
#include <QException>

class QSqlDatabase;

class SqlCipherUnavailableException : QException
{
public:
    void raise() const override { throw *this; }
    SqlCipherUnavailableException* clone() const override { return new SqlCipherUnavailableException(*this); }
};

class SqlCipherWrongPasswordException : QException
{
public:
    void raise() const override { throw *this; }
    SqlCipherWrongPasswordException* clone() const override { return new SqlCipherWrongPasswordException(*this); }
};

class PersonRecord
{
private:
    static QSqlDatabase openDatabase(const QString&, const QString&);
public:
    std::string name;
    std::string surname;
    unsigned int age;

    static std::vector<PersonRecord> allPersons;

    static void addPerson(const char*, const char*, const unsigned int);
    static void removePerson(const unsigned int);

    static void writeToFile(const QString&, const QString&);
    static void readFromFile(const QString&, const QString&);
};

#endif // PERSONRECORD_HPP
