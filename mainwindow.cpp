/*************************************************************************************
 * Copyright (c) 2023 Daniel Steinhauer                                              *
 *                                                                                   *
 * Permission is hereby granted, free of charge, to any person obtaining a copy      *
 * of this software and associated documentation files (the "Software"), to deal     *
 * in the Software without restriction, including without limitation the rights      *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         *
 * copies of the Software, and to permit persons to whom the Software is             *
 * furnished to do so, subject to the following conditions:                          *
 *                                                                                   *
 * The above copyright notice and this permission notice shall be included in all    *
 * copies or substantial portions of the Software.                                   *
 *                                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     *
 * SOFTWARE.                                                                         *
 *************************************************************************************/

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>

#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include "personrecord.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , nrec(this)
{
    ui->setupUi(this);

    // The NewRecordDialog will emit a signal with the newly created record.
    connect(&nrec, SIGNAL(producePersonRecord(PersonRecord)), this, SLOT(addRecord(PersonRecord)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refresh()
{
    ui->peopleTable->clear();
    ui->peopleTable->setColumnCount(3);
    ui->peopleTable->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Surname")));
    ui->peopleTable->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Name")));
    ui->peopleTable->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Age")));
    ui->peopleTable->setRowCount(PersonRecord::allPersons.size());
    for (unsigned int ii = 0; ii < PersonRecord::allPersons.size(); ++ii) {
        PersonRecord& pr = PersonRecord::allPersons.at(ii);
        ui->peopleTable->setItem(ii, 0, new QTableWidgetItem(QString::fromStdString(pr.surname)));
        ui->peopleTable->setItem(ii, 1, new QTableWidgetItem(QString::fromStdString(pr.name)));
        ui->peopleTable->setItem(ii, 2, new QTableWidgetItem(QString::number(pr.age)));
    }
}


void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_addButton_clicked()
{
    nrec.show();
}

void MainWindow::addRecord(PersonRecord pr)
{
    PersonRecord::allPersons.push_back(pr);
    refresh();
}

void MainWindow::on_peopleTable_cellActivated()
{
    ui->deleteButton->setEnabled(true);
}


void MainWindow::on_deleteButton_clicked()
{
    PersonRecord::removePerson(ui->peopleTable->currentRow());
    ui->deleteButton->setEnabled(false);
    refresh();
}


void MainWindow::on_actionSave_triggered()
{
    QUrl filePath = QFileDialog::getSaveFileUrl(
        this,
        tr("Save to..."),
        QUrl(),
        tr("Database files (*.db)")
    );

    // If the user clicked on cancel, don't do anything.
    if (filePath.isEmpty()) {
        return;
    }

    QString password = QInputDialog::getText(this, tr("Database password"), tr("Enter the database password:"), QLineEdit::Password);

    // If no password was provided or the user clicked on cancel, don't do anything.
    if (password.isEmpty()) {
        return;
    }

    try {
        PersonRecord::writeToFile(filePath.path(), password);
        statusBar()->showMessage(tr("Saved to %1").arg(filePath.fileName()));
    } catch (SqlCipherUnavailableException& exc) {
        QMessageBox::critical(this, tr("SQLCipher Unavailable"), tr("Could not load the SQLCipher driver."));
        statusBar()->showMessage(tr("Error while saving to file..."));
    } catch (SqlCipherWrongPasswordException& exc) {
        QMessageBox::critical(this, tr("Invalid File"), tr("This is not a valid file."));
        statusBar()->showMessage(tr("Error while saving to file..."));
    }
}


void MainWindow::on_actionOpen_triggered()
{
    QUrl filePath = QFileDialog::getOpenFileUrl(
        this,
        tr("Open database..."),
        QUrl(),
        tr("Database files (*.db)")
    );

    // If the user clicked on cancel, don't do anything.
    if (filePath.isEmpty()) {
        return;
    }

    QString password = QInputDialog::getText(this, tr("Database password"), tr("Enter the database password:"), QLineEdit::Password);

    // If no password was provided or the user clicked on cancel, don't do anything.
    if (password.isEmpty()) {
        return;
    }

    try {
        PersonRecord::readFromFile(filePath.path(), password);
        statusBar()->showMessage(tr("Opened file %1").arg(filePath.fileName()));
        refresh();
    } catch (SqlCipherUnavailableException& exc) {
        QMessageBox::critical(this, tr("SQLCipher Unavailable"), tr("Could not load the SQLCipher driver."));
        statusBar()->showMessage(tr("Error while opening file..."));
    } catch (SqlCipherWrongPasswordException& exc) {
        QMessageBox::critical(this, tr("Wrong Password"), tr("Either the password is wrong, or this is not a valid file."));
        statusBar()->showMessage(tr("Error while opening file..."));
    }
}

