/*************************************************************************************
 * Copyright (c) 2023 Daniel Steinhauer                                              *
 *                                                                                   *
 * Permission is hereby granted, free of charge, to any person obtaining a copy      *
 * of this software and associated documentation files (the "Software"), to deal     *
 * in the Software without restriction, including without limitation the rights      *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         *
 * copies of the Software, and to permit persons to whom the Software is             *
 * furnished to do so, subject to the following conditions:                          *
 *                                                                                   *
 * The above copyright notice and this permission notice shall be included in all    *
 * copies or substantial portions of the Software.                                   *
 *                                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     *
 * SOFTWARE.                                                                         *
 *************************************************************************************/

#include <stdexcept>
#include <sstream>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include "personrecord.hpp"

std::vector<PersonRecord> PersonRecord::allPersons = std::vector<PersonRecord> ();

void PersonRecord::addPerson(const char* pname, const char* psurname, const unsigned int page)
{
    PersonRecord nPerson;
    nPerson.name = std::string(pname);
    nPerson.surname = std::string(psurname);
    nPerson.age = page;

    PersonRecord::allPersons.push_back(nPerson);
}

void PersonRecord::removePerson(const unsigned int index)
{
    if (index >= PersonRecord::allPersons.size()) {
        std::stringstream err;
        err << "Cannot access element " << index << " from a list of " << PersonRecord::allPersons.size() << " entries.";
        throw std::range_error(err.str());
    }

    PersonRecord::allPersons.erase(PersonRecord::allPersons.begin() + index);
}

QSqlDatabase PersonRecord::openDatabase(const QString& filepath, const QString& password)
{
    // Check whether the Qt5 driver for SQLCipher is available.
    if (!QSqlDatabase::isDriverAvailable("QSQLCIPHER")) {
        throw SqlCipherUnavailableException();
    }

    // Open a database with it.
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLCIPHER", "con");
    db.setDatabaseName(filepath);
    db.open();

    // Set the password.
    db.exec(QString("PRAGMA key='%1'").arg(password));

    // Just a sample query to verify that everything works.
    // If the password was wrong or the file is invalid, this will fail.
    db.exec("SELECT * FROM PersonRecords LIMIT 1;");
    QSqlError error = db.lastError();
    if (error.isValid()) {
        throw SqlCipherWrongPasswordException();
    }

    return db;
}

void PersonRecord::writeToFile(const QString& filepath, const QString& password)
{
    QSqlDatabase db = PersonRecord::openDatabase(filepath, password);
    db.exec("DROP TABLE IF EXISTS PersonRecords;");
    db.exec("CREATE TABLE PersonRecords(Surname TEXT, Name TEXT, Age INT);");
    for (PersonRecord& pr : allPersons) {
        QSqlQuery insertPR(db);
        insertPR.prepare("INSERT INTO PersonRecords (Surname, Name, Age) VALUES (?, ?, ?);");
        insertPR.addBindValue(QString::fromStdString(pr.surname));
        insertPR.addBindValue(QString::fromStdString(pr.name));
        insertPR.addBindValue(pr.age);
        insertPR.exec();
    }
    db.close();
}

void PersonRecord::readFromFile(const QString& filepath, const QString& password)
{
    QSqlDatabase db = PersonRecord::openDatabase(filepath, password);

    allPersons.clear();
    QSqlQuery selectPR (db);
    selectPR.exec("SELECT Surname, Name, Age FROM PersonRecords;");
    while (selectPR.next()) {
        QString surname = selectPR.value(0).toString();
        QString name = selectPR.value(1).toString();
        unsigned int age = selectPR.value(2).toUInt();
        PersonRecord::addPerson(name.toStdString().c_str(), surname.toStdString().c_str(), age);
    }
    db.close();
}
