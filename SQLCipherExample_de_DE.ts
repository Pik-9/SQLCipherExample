<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>SQLCipher Database</source>
        <translatorcomment>Title - No Need To Translate</translatorcomment>
        <translation>SQLCipher Database</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="24"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Add</source>
        <translation>Neuen Eintrag</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Delete</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="69"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="81"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="53"/>
        <source>Surname</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="54"/>
        <source>Name</source>
        <translation>(Nach-)Name</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="55"/>
        <source>Age</source>
        <translation>Alter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="100"/>
        <source>Save to...</source>
        <translation>Speichern als...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="102"/>
        <location filename="mainwindow.cpp" line="136"/>
        <source>Database files (*.db)</source>
        <translation>Datenbankdateien (*.db)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="144"/>
        <source>Database password</source>
        <translation>Datenbank Passwort</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="144"/>
        <source>Enter the database password:</source>
        <translation>Datanbankpasswort eingeben:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="119"/>
        <source>Saved to %1</source>
        <translation>Gespeichert unter %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="121"/>
        <location filename="mainwindow.cpp" line="156"/>
        <source>SQLCipher Unavailable</source>
        <translation>SQLCipher Nicht Gefunden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="121"/>
        <location filename="mainwindow.cpp" line="156"/>
        <source>Could not load the SQLCipher driver.</source>
        <translation>Konnte SQLCipher Treiber nicht laden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="122"/>
        <location filename="mainwindow.cpp" line="125"/>
        <source>Error while saving to file...</source>
        <translation>Fehler beim Speichern der Datei...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>Invalid File</source>
        <translation>Fehlerhafte Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>This is not a valid file.</source>
        <translation>Diese Datei ist fehlerhaft.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <source>Open database...</source>
        <translation>Datenbank öffnen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <source>Opened file %1</source>
        <translation>%1 geöffnet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="160"/>
        <source>Error while opening file...</source>
        <translation>Fehler beim Öffnen der Datei...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Wrong Password</source>
        <translation>Falsches Passwort</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Either the password is wrong, or this is not a valid file.</source>
        <translation>Entweder ist das Passwort falsch oder die Datei ist fehlerhaft.</translation>
    </message>
</context>
<context>
    <name>NewRecordDialog</name>
    <message>
        <location filename="newrecorddialog.ui" line="14"/>
        <source>New Person Record</source>
        <translation>Neuen Personeneintrag</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="20"/>
        <source>Surname:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="30"/>
        <source>Name:</source>
        <translation>(Nach-)Name:</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="40"/>
        <source>Age:</source>
        <translation>Alter:</translation>
    </message>
</context>
</TS>
